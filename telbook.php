<?php
//Открываем фай с json
$file = file_get_contents("./telbook.json");
//Парсим
$Arr = json_decode($file,true);
?>
<!--HTML код -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Телефонная книга</title>
</head>
<body>
<table border="1">
    <tr>
        <th>Имя</th>
        <th>Фамилия</th>
        <th>Адрес</th>
        <th>Телефон</th>
    </tr>
    <?php
        foreach ($Arr as $value)
        {
            echo "<tr>".
                "<td>".$value['firstName']."</td>".
                "<td>".$value['lastName']."</td>".
                "<td>".$value['address']."</td>".
                "<td>".$value['phoneNumber']."</td>"
                ."</tr>";
        }

         ?>
</table>
</body>
</html>
